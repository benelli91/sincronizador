#!/usr/bin/env python​

import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime
import sys
sys.path.append(".")
import config
import pandas as pd
from Models.HU import HU

colorArray = ['{color:#333333}','{color:#d04437}','{color:#f79232}','{color:#14892c}','{color:#654982}']

huListFile = pd.read_excel (config.pathToExcel)
huList = list()


def getElement(driver, type, value):
    while (True):
        try:
            if type == 'id':
                element = driver.find_element_by_id(value)
            elif type == 'active':
                time.sleep(2)
                element = driver.switch_to_active_element()
            break
        except:
            time.sleep(1)
    return element

def getElements(driver, type, value):
    while (True):
        try:
            if type == 'tag':
                elements = driver.find_elements_by_tag_name(value)
            break
        except:
            time.sleep(1)
    return elements


if __name__ == "__main__":
    options = webdriver.ChromeOptions()
    options.add_argument("--start-maximized")
    #options.add_argument('headless')  #no muestra el navegador al ejecutar
    driver = webdriver.Chrome(config.chromeWebDriverDir+ 'chromedriver'+config.chromeWebDriverExtension, chrome_options=options)
    
    ### LOGIN ###
    driver.get(config.urlLogin)
    getElement(driver, "id", "i0116").send_keys(config.userVSTS)
    getElement(driver, "id", "idSIButton9").click()

    getElement(driver, "id", "i0118").send_keys(config.passwordVSTS)
    
    while (True):
        try:
            button = getElement(driver, "id", "idSIButton9")
            button.click()
            break
        except:
            pass
    
    line = 0    
    for hu in huListFile['Link']:
        huObject = HU()
        #huObject.hu = huListFile['HU'][line]
        huObject.link = config.VSTSurlBase + str(huListFile['Link'][line])
        huObject.resumen = config.prefix + huListFile['Epic link'][line] + "-" +huListFile['Resumen'][line] + config.sufix + str(huListFile['Link'][line])
        if huListFile['Etiquetas'][line] != None:
            huObject.etiquetas = huListFile['Etiquetas'][line].split("|")
        huObject.sp = huListFile['SP'][line]
        huObject.epic = huListFile['Epic link'][line]
        if huListFile['Components'][line] != None:
            huObject.components = huListFile['Components'][line].split("|")
        huObject.responsable = huListFile['Responsable'][line]
        huObject.tipoIncidencia = huListFile['Tipo'][line]
        
        ### VSTS ###
        driver.get(config.VSTSurlBase + str(hu))
        tables = []
        while (True):
            tables = getElements(driver, 'tag', "tbody")
            if len(tables) < 2:
                print('encontro menos de 2 tablas')
            else:
                print('Encontro todas las tablas')
                break

        if len(tables) == 4:
            primeraTabla = tables[1]
            segundaTabla = tables[3]
        elif len(tables) == 2:
            primeraTabla = tables[0]
            segundaTabla = tables[1]
        elif len(tables) == 3:
            primeraTabla = tables[1]
            segundaTabla = tables[2]
            #print(primeraTabla.text)
            #print(segundaTabla.text)
        else:
            c = 0
            print('Largo de tabla desconocido:',len(tables))
        
        
        data_1 = []
        index = 0
        array_width_1 = []
        
        header = primeraTabla.find_elements(By.TAG_NAME, "tr")
        for q in header:
            cells = q.find_elements(By.TAG_NAME, "td")
            row = []
            pos = 0
            for c in cells:   
                if index == 0:
                    array_width_1.append(c.get_attribute("width"))
                else:
                    while c.get_attribute("width") != "" and c.get_attribute("width") != array_width_1[pos]:
                        row.append(data_1[index-1][pos])
                        pos += 1
                row.append(c.text)
                pos += 1
            data_1.append(row)
            index += 1        
        
        data_2 = []
        index = 0
        array_width_2 = []
        body = segundaTabla.find_elements(By.TAG_NAME, "tr")
        for q in body:
            cells = q.find_elements(By.TAG_NAME, "td")
            
            row = []
            pos = 0

            if index == 0:
                for c in cells:  
                    array_width_2.append(c.get_attribute("width"))
                    row.append(c.text)
            else:
                #print("len(cells)",len(cells))
                for c in cells:
                    if len(cells) > pos or True:
                        #print("c.text",c.text)
                        #print("width",c.get_attribute("width"))
                        #print("index",index)
                        #print("pos",pos)
                        
                        if c.get_attribute("width") != "":    
                            while c.get_attribute("width") != array_width_2[pos]:
                                row.append(data_2[index-1][pos])
                                #print("Tomo de la linea anterior la posición", pos,data_2[index-1][pos])
                                pos += 1
                        #input("")
                        row.append(c.text)
                        pos += 1
                while pos < len(array_width_2):
                    #print(index,pos)
                    row.append(data_2[index-1][pos])
                    #print("Tomo de la linea anterior la posición", pos,data_2[index-1][pos])
                    pos += 1
                
            if row != []:
                #print("----------------------------")
                data_2.append(row)
                index += 1
        #input()
        huObject.table1=data_1
        huObject.table2=data_2
        huList.append(huObject)
        line += 1
    
    
    driver.get("https://technisys.atlassian.net/secure/RapidBoard.jspa?rapidView=515&projectKey=SVAGREMP")
    goToGoogle = getElement(driver, "id", "google-auth-button").click()
    getElement(driver, 'active', '').send_keys(config.userJIRA)
    getElement(driver, 'active', '').send_keys(Keys.ENTER)
    getElement(driver, 'active', '').send_keys(config.passwordJIRA)
    getElement(driver, 'active', '').send_keys(Keys.ENTER)
    #verificarionCode = input("JUANCITO ES EL UNO:")
    #verificarionCode = input("Ingrese codigo recibido por sms:")
    #driver.switch_to_active_element().send_keys(verificarionCode)
    #driver.switch_to_active_element().send_keys(Keys.ENTER)
    
    
    tipoHU = True
    for hu in huList:
        
        while (True):
            try:
                getElement(driver, "id", "createGlobalItem").click()
                break
            except:
                pass
        data_1 = hu.table1
        data_2 = hu.table2
        
        getElement(driver, "id", "description")
        

        
        pos = 0
        for h in data_1[0]:
            text= h
            detail = data_1[1][pos]
            getElement(driver, "id", "description").send_keys('*'+text.strip()+'*')
            getElement(driver, "id", "description").send_keys("\n")
            getElement(driver, "id", "description").send_keys(detail.strip())
            getElement(driver, "id", "description").send_keys("\n")
            getElement(driver, "id", "description").send_keys("\n")
            pos += 1
            
        isHeader = True
        numEscenario = 0
        for row in data_2:
            if isHeader == False:
                pos = 0
                arrayPos = numEscenario % len(colorArray)
                getElement(driver, "id", "description").send_keys(colorArray[arrayPos])
                for cell in row:
                    text= data_2[0][pos]
                    detail = cell
                    getElement(driver, "id", "description").send_keys('*'+text.strip()+'*')
                    getElement(driver, "id", "description").send_keys("\n")
                    getElement(driver, "id", "description").send_keys(detail)
                    getElement(driver, "id", "description").send_keys("\n")
                    getElement(driver, "id", "description").send_keys("\n")
                    
                    pos += 1
                getElement(driver, "id", "description").send_keys('{color}')
                numEscenario += 1
            else:
                isHeader = False
        
        getElement(driver, "id", "description").send_keys("\n")
        getElement(driver, "id", "description").send_keys('*Link a VSTS:*')
        getElement(driver, "id", "description").send_keys("\n")
        getElement(driver, "id", "description").send_keys(hu.link)
        
        if tipoHU:
            getElement(driver, "id", "issuetype-field").click()
            getElement(driver, "id", "issuetype-field").send_keys(Keys.BACK_SPACE)
            getElement(driver, "id", "issuetype-field").send_keys(hu.tipoIncidencia)
            getElement(driver, "id", "issuetype-field").send_keys(Keys.ENTER)
            tipoHU = False
            
        getElement(driver, "id", "summary")
        getElement(driver, "id", "summary").click()
        while (True):
            try:
                getElement(driver, "id", "summary").send_keys(hu.resumen)
                break
            except:
                pass
        
        
        for tag in hu.etiquetas:
            getElement(driver, "id", "labels-textarea").send_keys(tag)
            getElement(driver, "id", "labels-textarea").send_keys(Keys.SPACE)
        
        getElement(driver, "id", "customfield_10004").send_keys(str(hu.sp))
        
        getElement(driver, "id", "customfield_10300-field").send_keys(hu.epic)

        getElement(driver, 'active', '').send_keys(Keys.ARROW_DOWN)
        getElement(driver, 'active', '').send_keys(Keys.ENTER)
        
        
        for component in hu.components:
            getElement(driver, "id", "components-textarea").send_keys(component)
            getElement(driver, 'active', '').send_keys(Keys.ENTER)
        
        if hu.responsable != "":
            getElement(driver, "id", "assignee-field").click()
            for x in range(10):
                getElement(driver, "id", "assignee-field").send_keys(Keys.BACK_SPACE)
            getElement(driver, "id", "assignee-field").click()
            getElement(driver, "id", "assignee-field").send_keys(hu.responsable)
            
            getElement(driver, 'active', '').send_keys(Keys.ENTER)
        
        getElement(driver, "id", "create-issue-submit").click()
        
    getElement(driver, "id", "create-issue-submit")




