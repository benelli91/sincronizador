#!/usr/bin/env python​

import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime
import sys
sys.path.append(".")
import config
import pandas as pd
from Models.HU import HU

colorArray = ['{color:#333333}','{color:#d04437}','{color:#f79232}','{color:#14892c}','{color:#654982}']

huListFile = pd.read_excel (config.pathToExcel)
huList = list()

if __name__ == "__main__":
    options = webdriver.ChromeOptions()
    options.add_argument("--start-maximized")
    #options.add_argument('headless')  #no muestra el navegador al ejecutar
    driver = webdriver.Chrome(config.chromeWebDriverDir+ 'chromedriver'+config.chromeWebDriverExtension, chrome_options=options)
    wait = WebDriverWait(driver, 5)
    
    ### LOGIN ###
    try:
        driver.get(config.urlLogin)
        time.sleep(2)
        driver.find_element_by_id("i0116").send_keys(config.userVSTS)
        time.sleep(1)
        driver.find_element_by_id("idSIButton9").click()
    except:
        time.sleep(1)
        driver.find_element_by_id("idSIButton9").click()
        
    try:
        driver.find_element_by_id("i0118").send_keys(config.passwordVSTS)
        time.sleep(2)
        driver.find_element_by_id("idSIButton9").click()
    except:
        time.sleep(2)
        driver.find_element_by_id("idSIButton9").click()
    
    line = 0    
    for hu in huListFile['Link']:
        huObject = HU()
        #huObject.hu = huListFile['HU'][line]
        huObject.link = config.VSTSurlBase + str(huListFile['Link'][line])
        huObject.resumen = config.prefix + huListFile['Epic link'][line] + "-" +huListFile['Resumen'][line] + config.sufix + str(huListFile['Link'][line])
        if huListFile['Etiquetas'][line] != None:
            huObject.etiquetas = huListFile['Etiquetas'][line].split("|")
        huObject.sp = huListFile['SP'][line]
        huObject.epic = huListFile['Epic link'][line]
        if huListFile['Components'][line] != None:
            huObject.components = huListFile['Components'][line].split("|")
        huObject.responsable = huListFile['Responsable'][line]
        huObject.tipoIncidencia = huListFile['Tipo'][line]
        
        ### VSTS ###
        driver.get(config.VSTSurlBase + str(hu))
        time.sleep(7)
        tables = []
        tables = wait.until(EC.presence_of_all_elements_located((By.TAG_NAME, ("tbody"))))
        if len(tables) == 4:
            primeraTabla = tables[1]
            segundaTabla = tables[3]
        elif len(tables) == 2:
            primeraTabla = tables[0]
            segundaTabla = tables[1]
        elif len(tables) == 3:
            primeraTabla = tables[1]
            segundaTabla = tables[2]
            #print(primeraTabla.text)
            #print(segundaTabla.text)
        else:
            c = 0
            print('Largo de tabla desconocido:',len(tables))
        
        
        data_1 = []
        index = 0
        array_width_1 = []
        
        header = primeraTabla.find_elements(By.TAG_NAME, "tr")
        for q in header:
            cells = q.find_elements(By.TAG_NAME, "td")
            row = []
            pos = 0
            for c in cells:   
                if index == 0:
                    array_width_1.append(c.get_attribute("width"))
                else:
                    while c.get_attribute("width") != "" and c.get_attribute("width") != array_width_1[pos]:
                        row.append(data_1[index-1][pos])
                        pos += 1
                row.append(c.text)
                pos += 1
            data_1.append(row)
            index += 1        
        
        data_2 = []
        index = 0
        array_width_2 = []
        body = segundaTabla.find_elements(By.TAG_NAME, "tr")
        for q in body:
            cells = q.find_elements(By.TAG_NAME, "td")
            
            row = []
            pos = 0
            if index == 0:
                for c in cells:  
                    array_width_2.append(c.get_attribute("width"))
                    row.append(c.text)
            else:
                while pos < len(array_width_2):
                    if len(cells) > pos:
                        c = cells[pos]
                        #print(c.text)
                        #print(c.get_attribute("width"))
                        #print(index)
                        #print("pos",pos)
                        #input("")
                        if c.get_attribute("width") != "":    
                            while c.get_attribute("width") != array_width_2[pos]:
                                row.append(data_2[index-1][pos])
                                pos += 1
                        row.append(c.text)
                    else:
                        print(index,pos)
                        row.append(data_2[index-1][pos])
                    pos += 1
                
            if row != []:
                #print("----------------------------")
                data_2.append(row)
                index += 1
            
        huObject.table1=data_1
        huObject.table2=data_2
        huList.append(huObject)
        line += 1
    
    
    driver.get("https://technisys.atlassian.net/secure/RapidBoard.jspa?rapidView=515&projectKey=SVAGREMP")
    goToGoogle = driver.find_element_by_id("google-signin-button").click()
    time.sleep(2)
    driver.switch_to_active_element().send_keys(config.userJIRA)
    driver.switch_to_active_element().send_keys(Keys.ENTER)
    time.sleep(2)
    driver.switch_to_active_element().send_keys(config.passwordJIRA)
    driver.switch_to_active_element().send_keys(Keys.ENTER)
    #verificarionCode = input("JUANCITO ES EL UNO:")
    #verificarionCode = input("Ingrese codigo recibido por sms:")
    #driver.switch_to_active_element().send_keys(verificarionCode)
    #driver.switch_to_active_element().send_keys(Keys.ENTER)
    
    
    time.sleep(3)
    for hu in huList:
        time.sleep(5)
        wait.until(EC.presence_of_all_elements_located((By.ID, ("createGlobalItem"))))
        driver.find_element_by_id("createGlobalItem").click()
        time.sleep(5)
        data_1 = hu.table1
        data_2 = hu.table2
        
        wait.until(EC.presence_of_all_elements_located((By.ID, ("description"))))
        pos = 0
        for h in data_1[0]:
            text= h
            detail = data_1[1][pos]
            driver.find_element_by_id("description").send_keys('*'+text.strip()+'*')
            driver.find_element_by_id("description").send_keys("\n")
            driver.find_element_by_id("description").send_keys(detail.strip())
            driver.find_element_by_id("description").send_keys("\n")
            driver.find_element_by_id("description").send_keys("\n")
            pos += 1
            
        isHeader = True
        numEscenario = 0
        for row in data_2:
            if isHeader == False:
                pos = 0
                arrayPos = numEscenario % len(colorArray)
                driver.find_element_by_id("description").send_keys(colorArray[arrayPos])
                for cell in row:
                    text= data_2[0][pos]
                    detail = cell
                    driver.find_element_by_id("description").send_keys('*'+text.strip()+'*')
                    driver.find_element_by_id("description").send_keys("\n")
                    driver.find_element_by_id("description").send_keys(detail)
                    driver.find_element_by_id("description").send_keys("\n")
                    driver.find_element_by_id("description").send_keys("\n")
                    
                    pos += 1
                driver.find_element_by_id("description").send_keys('{color}')
                numEscenario += 1
            else:
                isHeader = False
        
        driver.find_element_by_id("description").send_keys("\n")
        driver.find_element_by_id("description").send_keys('*Link a VSTS:*')
        driver.find_element_by_id("description").send_keys("\n")
        driver.find_element_by_id("description").send_keys(hu.link)
                
        driver.find_element_by_id("summary").send_keys(hu.resumen)
        for tag in hu.etiquetas:
            driver.find_element_by_id("labels-textarea").send_keys(tag)
            driver.switch_to_active_element().send_keys(Keys.SPACE)
        
        driver.find_element_by_id("customfield_10004").send_keys(str(hu.sp))
        
        driver.find_element_by_id("customfield_10300-field").send_keys(hu.epic)
        time.sleep(2)
        driver.switch_to_active_element().send_keys(Keys.ARROW_DOWN)
        driver.switch_to_active_element().send_keys(Keys.ENTER)
        
        
        for component in hu.components:
            driver.find_element_by_id("components-textarea").send_keys(component)
            driver.switch_to_active_element().send_keys(Keys.ENTER)
        
        if hu.responsable != "":
            driver.find_element_by_id("assignee-field").click()
            for x in range(10):
                driver.find_element_by_id("assignee-field").send_keys(Keys.BACK_SPACE)
            driver.find_element_by_id("assignee-field").click()
            driver.find_element_by_id("assignee-field").send_keys(hu.responsable)
            time.sleep(1)
            driver.switch_to_active_element().send_keys(Keys.ENTER)
            
        driver.find_element_by_id("issuetype-field").click()
        driver.find_element_by_id("issuetype-field").send_keys(Keys.BACK_SPACE)
        driver.find_element_by_id("issuetype-field").send_keys(hu.tipoIncidencia)
        time.sleep(1)
        driver.find_element_by_id("issuetype-field").send_keys(Keys.ENTER)
        
        
        time.sleep(3)
        driver.find_element_by_id("create-issue-submit").click()
        time.sleep(5)
    
    wait.until(EC.presence_of_all_elements_located((By.ID, ("createGlobalItem"))))